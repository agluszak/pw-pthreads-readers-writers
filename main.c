#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <stdarg.h>
#include <string.h>

#define READERS 10
#define WRITERS 2
#define NAP 5
#define BSIZE 32

void syserr(int bl, const char* fmt, ...) {
    va_list fmt_args;

    fprintf(stderr, "ERROR: ");

    va_start(fmt_args, fmt);
    vfprintf(stderr, fmt, fmt_args);
    va_end (fmt_args);
    fprintf(stderr, " (%d; %s)\n", bl, strerror(bl));
    exit(1);
}

struct readwrite {
    pthread_mutex_t lock;
    pthread_cond_t readers;
    pthread_cond_t writers;
    int rcount, wcount, rwait, wwait;
    int change;
};

struct readwrite library;
char book[BSIZE];
int working = 1;

/* Initialize a buffer */

void debug_print(struct readwrite* rw, char* msg1, char* msg2) {
    printf("%s %lu %s. rc: %d, rw: %d, wc: %d, ww: %d\n", msg1, pthread_self() % 113, msg2, rw->rcount, rw->rwait,
           rw->wcount, rw->wwait);
}

void init(struct readwrite* rw) {
    int err;

    if ((err = pthread_mutex_init(&rw->lock, 0)) != 0)
        syserr(err, "mutex init failed");
    if ((err = pthread_cond_init(&rw->readers, 0)) != 0)
        syserr(err, "cond init 1 failed");
    if ((err = pthread_cond_init(&rw->writers, 0)) != 0)
        syserr(err, "cond init 2 failed");
    rw->rcount = 0;
    rw->wcount = 0;
    rw->rwait = 0;
    rw->wwait = 0;
    rw->change = 0;
}

/* Destroy the buffer */

void destroy(struct readwrite* rw) {
    int err;

    if ((err = pthread_cond_destroy(&rw->writers)) != 0)
        syserr(err, "cond destroy 1 failed");
    if ((err = pthread_cond_destroy(&rw->readers)) != 0)
        syserr(err, "cond destroy 2 failed");
    if ((err = pthread_mutex_destroy(&rw->lock)) != 0)
        syserr(err, "mutex destroy failed");
}

void start_reading(struct readwrite* rw) {
    int err;

    if ((err = pthread_mutex_lock(&rw->lock)) != 0)
        syserr(err, "lock failed");
    rw->rwait++;
    debug_print(&library, "reader", "starts");
    while (rw->wcount > 0 || rw->wwait > 0) {
        debug_print(&library, "reader", "sleeps");
        if ((err = pthread_cond_wait(&rw->readers, &rw->lock)) != 0)
            syserr(err, "cond wait failed");
        if (rw->change == 1 && rw->wcount == 0) { //jesli obudził nas pisarz
            rw->change = 0;
            break;
        }
    }
    debug_print(&library, "reader", "wakes up");
    rw->rwait--;
    rw->rcount++;
    if ((err = pthread_cond_signal(&rw->readers)) != 0)
        syserr(err, "cond signal failed");
    if ((err = pthread_mutex_unlock(&rw->lock)) != 0)
        syserr(err, "unlock failed");
}

void finish_reading(struct readwrite* rw) {
    int err;

    if ((err = pthread_mutex_lock(&rw->lock)) != 0)
        syserr(err, "lock failed");
    rw->rcount--;
    debug_print(&library, "reader", "finished");
    if (rw->rcount == 0) {
        if ((err = pthread_cond_signal(&rw->writers)) != 0)
            syserr(err, "cond signal failed");
    }
    if ((err = pthread_mutex_unlock(&rw->lock)) != 0)
        syserr(err, "unlock failed");
}

void* reader(void* data) {
    while (working) {
        start_reading(&library);
        debug_print(&library, "reader", book);
        finish_reading(&library);
        sleep(1);
    }
    return 0;
}

void start_writing(struct readwrite* rw) {
    int err;

    if ((err = pthread_mutex_lock(&rw->lock)) != 0)
        syserr(err, "lock failed");
    rw->wwait++;
    debug_print(&library, "writer", "starts");
    while (rw->rcount > 0 || rw->wcount > 0 || rw->change == 1) {
        debug_print(&library, "writer", "sleeps");
        if ((err = pthread_cond_wait(&rw->writers, &rw->lock)) != 0)
            syserr(err, "cond wait failed");
    }
    debug_print(&library, "writer", "wakes up");
    rw->wwait--;
    rw->wcount++;
    if ((err = pthread_mutex_unlock(&rw->lock)) != 0)
        syserr(err, "unlock failed");
}

void finish_writing(struct readwrite* rw) {
    int err;

    if ((err = pthread_mutex_lock(&rw->lock)) != 0)
        syserr(err, "lock failed");
    rw->wcount--;

    if (rw->rwait > 0) {
        debug_print(&library, "writer", "finishes and wakes up a reader");
        rw->change = 1;
        if ((err = pthread_cond_signal(&rw->readers)) != 0)
            syserr(err, "cond signal failed");
    } else {
        debug_print(&library, "writer", "finishes and wakes up a writer");
        if ((err = pthread_cond_signal(&rw->writers)) != 0)
            syserr(err, "cond signal failed");
    }
    if ((err = pthread_mutex_unlock(&rw->lock)) != 0)
        syserr(err, "unlock failed");
}

void* writer(void* data) {
    int l;
    while (working) {

        start_writing(&library);
        debug_print(&library, "writer", "writes");
        l = rand() % 10;
        snprintf(book, BSIZE, "6 times a number %d %d %d %d %d %d", l, l, l, l, l, l);
        finish_writing(&library);
        sleep(1);

    }
    return 0;
}


int main() {
    pthread_t th[READERS + WRITERS];
    pthread_attr_t attr;
    int err;
    void* retval;

    srand((unsigned) time(0));

    init(&library);

    if ((err = pthread_attr_init(&attr)) != 0)
        syserr(err, "attr_init failed");
    if ((err = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE)) != 0)
        syserr(err, "attr_setdetachstate failed");

    for (int i = 0; i < READERS; ++i) {
        if ((err = pthread_create(&th[i], &attr, reader, 0)) != 0)
            syserr(err, "create reader failed");
    }

    for (int i = READERS; i < READERS + WRITERS; ++i) {
        if ((err = pthread_create(&th[i], &attr, writer, 0)) != 0)
            syserr(err, "create writer failed");
    }

    sleep(NAP);
    printf("STOP\n");
    working = 0;

    for (int i = 0; i < READERS + WRITERS; ++i) {
        if ((err = pthread_join(th[i], &retval)) != 0)
            syserr(err, "join 1 failed");
    }
    printf("all joined\n");
    if ((err = pthread_attr_destroy(&attr)) != 0)
        syserr(err, "cond destroy failed");

    destroy(&library);
    return 0;
}
